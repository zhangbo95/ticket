package cn.kj0601.ticket.app.service.impl;

import cn.kj0601.ticket.base.entity.Job;
import cn.kj0601.ticket.base.dao.JobDao;
import cn.kj0601.ticket.app.service.JobService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 职务表 服务实现类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@Service
public class JobServiceImpl extends ServiceImpl<JobDao, Job> implements JobService {

}
