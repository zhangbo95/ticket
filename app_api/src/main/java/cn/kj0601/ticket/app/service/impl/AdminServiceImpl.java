package cn.kj0601.ticket.app.service.impl;

import cn.kj0601.ticket.base.entity.Admin;
import cn.kj0601.ticket.base.dao.AdminDao;
import cn.kj0601.ticket.app.service.AdminService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 员工表 服务实现类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminDao, Admin> implements AdminService {

}
