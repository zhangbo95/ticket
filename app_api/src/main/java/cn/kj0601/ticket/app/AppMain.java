package cn.kj0601.ticket.app;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 名称
 *
 * @Description :
 * @Author : zhangbo
 * @Date: 2021/12/14 12:18
 */
@ComponentScan("cn.kj0601.ticket")
@SpringBootApplication
@MapperScan("cn.kj0601.ticket.base.dao")
public class AppMain {

    public static void main(String[] args) {
        SpringApplication.run(AppMain.class,args);
    }
}
