package cn.kj0601.ticket.app.service;

import cn.kj0601.ticket.base.entity.Face;
import cn.kj0601.ticket.base.util.ResultJson;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 票面表 服务类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
public interface FaceService extends IService<Face> {

    //票务列表
    ResultJson selectTicket(Map<String,Object> map);


}
