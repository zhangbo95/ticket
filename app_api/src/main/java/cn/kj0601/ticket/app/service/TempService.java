package cn.kj0601.ticket.app.service;

import cn.kj0601.ticket.base.entity.Temp;
import cn.kj0601.ticket.base.util.ResultJson;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 订单临时表  服务类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
public interface TempService extends IService<Temp> {

    ResultJson addTemp(Map<String,Object> map);

}
