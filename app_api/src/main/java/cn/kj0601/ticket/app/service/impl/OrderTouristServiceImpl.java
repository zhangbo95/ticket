package cn.kj0601.ticket.app.service.impl;

import cn.kj0601.ticket.base.entity.OrderTourist;
import cn.kj0601.ticket.base.dao.OrderTouristDao;
import cn.kj0601.ticket.app.service.OrderTouristService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单游客关系表 服务实现类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@Service
public class OrderTouristServiceImpl extends ServiceImpl<OrderTouristDao, OrderTourist> implements OrderTouristService {

}
