package cn.kj0601.ticket.app.controller;


import cn.kj0601.ticket.app.service.NoticeService;
import cn.kj0601.ticket.base.util.ResultJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * <p>
 * 公告表 前端控制器
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@RestController
@RequestMapping("/notice")
public class NoticeController {

    @Autowired
    NoticeService noticeService;

    @PostMapping("info")
    public ResultJson selcet(@RequestParam Map<String,Object> map){


        return noticeService.select(map);

    }

}

