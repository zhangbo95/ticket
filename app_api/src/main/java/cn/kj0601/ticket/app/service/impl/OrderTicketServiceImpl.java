package cn.kj0601.ticket.app.service.impl;

import cn.kj0601.ticket.base.entity.OrderTicket;
import cn.kj0601.ticket.base.dao.OrderTicketDao;
import cn.kj0601.ticket.app.service.OrderTicketService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单票务关系表 服务实现类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@Service
public class OrderTicketServiceImpl extends ServiceImpl<OrderTicketDao, OrderTicket> implements OrderTicketService {

}
