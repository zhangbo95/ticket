package cn.kj0601.ticket.app.service;

import cn.kj0601.ticket.base.entity.OrderTicket;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单票务关系表 服务类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
public interface OrderTicketService extends IService<OrderTicket> {

}
