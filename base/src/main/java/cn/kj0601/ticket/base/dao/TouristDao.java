package cn.kj0601.ticket.base.dao;

import cn.kj0601.ticket.base.entity.Tourist;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.Map;

/**
 * <p>
 * 游客表 Mapper 接口
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
public interface TouristDao extends BaseMapper<Tourist> {
    //添加用户
    int insertTourist(Map<String,Object> map);

}
