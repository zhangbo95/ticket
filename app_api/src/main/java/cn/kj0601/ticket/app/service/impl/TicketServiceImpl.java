package cn.kj0601.ticket.app.service.impl;

import cn.kj0601.ticket.base.entity.Ticket;
import cn.kj0601.ticket.base.dao.TicketDao;
import cn.kj0601.ticket.app.service.TicketService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 票务 服务实现类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@Service
public class TicketServiceImpl extends ServiceImpl<TicketDao, Ticket> implements TicketService {

}
