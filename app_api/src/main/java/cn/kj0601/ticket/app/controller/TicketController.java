package cn.kj0601.ticket.app.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 票务 前端控制器
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@RestController
@RequestMapping("/ticket")
public class TicketController {

}

