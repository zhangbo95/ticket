package cn.kj0601.ticket.base.dao;

import cn.kj0601.ticket.base.entity.FaceAndScenic;
import cn.kj0601.ticket.base.entity.FaceScenic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 票和景点 Mapper 接口
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
public interface FaceScenicDao extends BaseMapper<FaceScenic> {

    FaceAndScenic selectGetList(Map<String,Object> map);

}
