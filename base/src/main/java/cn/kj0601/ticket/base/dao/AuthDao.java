package cn.kj0601.ticket.base.dao;

import cn.kj0601.ticket.base.entity.Auth;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
public interface AuthDao extends BaseMapper<Auth> {

}
