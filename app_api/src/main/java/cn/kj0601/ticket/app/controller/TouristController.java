package cn.kj0601.ticket.app.controller;


import cn.kj0601.ticket.app.service.TouristService;
import cn.kj0601.ticket.base.util.ResultJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * <p>
 * 游客表 前端控制器
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@RestController
@RequestMapping("/tourist")
public class TouristController {

    @Autowired
    TouristService touristService;

    //添加用户
    @PostMapping("add")
    public ResultJson addTourist(@RequestParam Map<String,Object> map){
        if(map.get("userId")==null||"".equals(map.get("userId"))
                ||map.get("cardNum")==null||"".equals(map.get("cardNum"))
                ||map.get("name")==null||"".equals(map.get("name"))
                ||map.get("phone")==null||"".equals(map.get("phone"))
        ){
            return  ResultJson.err(201,"用户id或游客身份证号未填入");
        }
        return touristService.addTourist(map);
    }

}

