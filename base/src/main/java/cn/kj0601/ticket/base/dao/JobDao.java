package cn.kj0601.ticket.base.dao;

import cn.kj0601.ticket.base.entity.Job;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 职务表 Mapper 接口
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
public interface JobDao extends BaseMapper<Job> {

}
