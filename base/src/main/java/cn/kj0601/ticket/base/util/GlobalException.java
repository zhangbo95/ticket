package cn.kj0601.ticket.base.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

/**
 * KJ0601
 *
 * @Description :
 * @Author : zhangbo
 * @Date: 2021/12/15 9:15
 */
@ControllerAdvice
@Slf4j
public class GlobalException {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResultJson handler(Exception e){
        log.error(e.getMessage());
        StringBuilder sb = new StringBuilder().append("\n\t");

        List list = Arrays.asList(e.getStackTrace());
        for (Object o : list) {
            sb.append(o.toString()+"\n\t\t");
        }
        log.error(sb.toString());

        return ResultJson.err(500,"服务器内部错误，请联系管理员");
    }
}


