package cn.kj0601.ticket.app.service;

import cn.kj0601.ticket.base.entity.Auto;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 管理 服务类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
public interface AutoService extends IService<Auto> {

}
