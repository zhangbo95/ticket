package cn.kj0601.ticket.app.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 订单票务关系表 前端控制器
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@RestController
@RequestMapping("/orderTicket")
public class OrderTicketController {

}

