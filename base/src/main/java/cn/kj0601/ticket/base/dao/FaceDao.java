package cn.kj0601.ticket.base.dao;

import cn.kj0601.ticket.base.entity.Face;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 票面表 Mapper 接口
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
public interface FaceDao extends BaseMapper<Face>{


}
