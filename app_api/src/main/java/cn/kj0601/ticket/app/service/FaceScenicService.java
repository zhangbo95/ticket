package cn.kj0601.ticket.app.service;

import cn.kj0601.ticket.base.entity.FaceScenic;
import cn.kj0601.ticket.base.util.ResultJson;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 票和景点 服务类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
public interface FaceScenicService extends IService<FaceScenic> {

    //内容介绍
    ResultJson selectTicket(Map<String,Object> map);

}
