package cn.kj0601.ticket.app.service.impl;

import cn.kj0601.ticket.base.entity.FaceAndScenic;
import cn.kj0601.ticket.base.entity.FaceScenic;
import cn.kj0601.ticket.base.dao.FaceScenicDao;
import cn.kj0601.ticket.app.service.FaceScenicService;
import cn.kj0601.ticket.base.util.ResultJson;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 票和景点 服务实现类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@Service
public class FaceScenicServiceImpl extends ServiceImpl<FaceScenicDao, FaceScenic> implements FaceScenicService {

    @Autowired
    FaceScenicDao faceScenicDao;

    @Override
    public ResultJson selectTicket(Map<String, Object> map) {
        FaceAndScenic faceAndScenic = faceScenicDao.selectGetList(map);
        if(faceAndScenic == null){
            return ResultJson.err(203,"没有当前票面");
        }

        return ResultJson.ok("查询成功",faceAndScenic);
    }
}
