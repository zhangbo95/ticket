package cn.kj0601.ticket.app.controller;


import cn.kj0601.ticket.app.service.TempService;
import cn.kj0601.ticket.base.util.ResultJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 订单临时表  前端控制器
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@RestController
@RequestMapping("/temp")
public class TempController {

    @Autowired
    TempService tempService;

    @PostMapping("addTemp")
    public ResultJson addTemp(@RequestParam Map<String,Object> map){

        if(map.get("face_id") ==null || "".equals(map.get("face_id"))){
            return ResultJson.err(203,"face_id未传递");
        }
        if(map.get("user_id") ==null || "".equals(map.get("user_id"))){
            return ResultJson.err(203,"user_id未传递");
        }


        return tempService.addTemp(map);

    }

}

