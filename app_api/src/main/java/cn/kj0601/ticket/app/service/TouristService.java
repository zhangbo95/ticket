package cn.kj0601.ticket.app.service;

import cn.kj0601.ticket.base.entity.Tourist;
import cn.kj0601.ticket.base.util.ResultJson;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 游客表 服务类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
public interface TouristService extends IService<Tourist> {

    //添加用户
    ResultJson addTourist(Map<String,Object> map);

}
