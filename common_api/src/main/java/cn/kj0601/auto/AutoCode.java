package cn.kj0601.auto;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * 名称
 *
 * @Description : 代码自动生成
 * @Author : zhangbo
 * @Date: 2021/12/14 11:01
 */
public class AutoCode {

    public static void main(String[] args) {
        //创建代码生成器
        AutoGenerator autoGenerator = new AutoGenerator();

        //完成全局配置
        GlobalConfig globalConfig = new GlobalConfig();
        globalConfig.setAuthor("kj0601");
        globalConfig.setMapperName("%sDao");
        globalConfig.setServiceName("%sService");
        globalConfig.setServiceImplName("%sServiceImpl");
        globalConfig.setControllerName("%sController");

        globalConfig.setOutputDir("d:\\myCode");
        globalConfig.setFileOverride(true);
        autoGenerator.setGlobalConfig(globalConfig);

        //配置数据源
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setDriverName("com.mysql.cj.jdbc.Driver");
        dataSourceConfig.setDbType(DbType.MYSQL);
        dataSourceConfig.setUrl("jdbc:mysql://127.0.0.1:3306/ticket_dev?serverTimezone=Asia/Shanghai");
        dataSourceConfig.setUsername("root");
        dataSourceConfig.setPassword("123456");
        autoGenerator.setDataSource(dataSourceConfig);

        //生成策略
        StrategyConfig strategyConfig = new StrategyConfig();
        strategyConfig.setTablePrefix("m_","o_","p_","t_","u_");
        strategyConfig.setNaming(NamingStrategy.underline_to_camel);
        strategyConfig.setEntityLombokModel(true);
        strategyConfig.setChainModel(true);
        strategyConfig.setRestControllerStyle(true);
        autoGenerator.setStrategy(strategyConfig);

        //包生成方式
        PackageConfig packageConfig = new PackageConfig();
        packageConfig.setParent("cn.kj0601.ticket");
        packageConfig.setMapper("base.dao");
        packageConfig.setEntity("base.entity");
        packageConfig.setService("app.service");
        packageConfig.setServiceImpl("app.service.impl");
        packageConfig.setController("app.controller");
        autoGenerator.setPackageInfo(packageConfig);

        //执行生成操作
        autoGenerator.execute();





    }
}
