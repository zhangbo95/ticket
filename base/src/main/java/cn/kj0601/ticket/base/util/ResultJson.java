package cn.kj0601.ticket.base.util;

import lombok.Data;
import lombok.Getter;

/**
 * 名称
 *
 * @Description :
 * @Author : zhangbo
 * @Date: 2021/12/14 12:20
 */
@Getter
public class ResultJson {
    private Integer Code;
    private String msg;
    private Object data;

    public ResultJson(Integer code, String msg, Object data) {
        Code = code;
        this.msg = msg;
        this.data = data;
    }

    public static ResultJson ok(String msg){
        return new ResultJson(200,msg,null);
    }

    public static ResultJson ok(Object data){
        return new ResultJson(200,"操作成功",data);
    }

    public static ResultJson ok(String msg,Object data){
        return new ResultJson(200,msg,data);
    }

    public static ResultJson err(Integer code,String msg){
        return new ResultJson(code,msg,null);
    }
}
