package cn.kj0601.ticket.base.util;

import java.util.UUID;

/**
 * KJ0601
 *
 * @Description :
 * @Author : Aedes
 * @Date: 2021/11/12 9:46
 */
public class UUIDUtil {

    public static String getUUID(){
        return UUID.randomUUID().toString().replace("-","");
    }

    public static void main(String[] args) {
        System.out.println(getUUID());
    }

}


