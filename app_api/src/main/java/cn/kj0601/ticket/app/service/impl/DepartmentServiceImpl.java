package cn.kj0601.ticket.app.service.impl;

import cn.kj0601.ticket.base.entity.Department;
import cn.kj0601.ticket.base.dao.DepartmentDao;
import cn.kj0601.ticket.app.service.DepartmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 部门表 服务实现类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentDao, Department> implements DepartmentService {

}
