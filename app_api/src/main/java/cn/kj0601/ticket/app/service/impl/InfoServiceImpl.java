package cn.kj0601.ticket.app.service.impl;

import cn.kj0601.ticket.base.entity.Info;
import cn.kj0601.ticket.base.dao.InfoDao;
import cn.kj0601.ticket.app.service.InfoService;
import cn.kj0601.ticket.base.util.ResultJson;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 信息表 服务实现类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@Service
public class InfoServiceImpl extends ServiceImpl<InfoDao, Info> implements InfoService {

    @Autowired
    InfoDao infoDao;

    //轮播图-通过时间倒序查询三个
    @Override
    public ResultJson SelectInquire(Map<String, Object> map) {

        QueryWrapper<Info> queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("create_time").eq("del_state",0).eq("type",map.get("type"));
        queryWrapper.last("limit 0,3");
        List<Info> list = infoDao.selectList(queryWrapper);
        return ResultJson.ok("轮播图查询成功",list);
    }


    //景点-查询最新的3个
    @Override
    public ResultJson selecetScenicspot(Map<String, Object> map) {
        QueryWrapper<Info> queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("update_time").eq("del_state",0).eq("type",map.get("type"));
        queryWrapper.last("limit 0,3");

        List<Info> list = infoDao.selectList(queryWrapper);
        return ResultJson.ok("景点查询成功",list);
    }

    //优惠信息-查询最新的2个
    @Override
    public ResultJson SelectDiscounts(Map<String, Object> map) {
        QueryWrapper<Info> queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("update_time").eq("del_state",0).eq("type",map.get("type"));
        queryWrapper.last("limit 0,2");
        List<Info> list = infoDao.selectList(queryWrapper);
        return ResultJson.ok("优惠活动查询成功",list);
    }
    //园区信息-查询最新的一个
    @Override
    public ResultJson SelectGarden(Map<String, Object> map) {
        QueryWrapper<Info> queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("update_time").eq("del_state",0).eq("type",map.get("type"));
        queryWrapper.last("limit 1");

        List<Info> list = infoDao.selectList(queryWrapper);
        return ResultJson.ok("园区信息查询成功",list);
    }

}
