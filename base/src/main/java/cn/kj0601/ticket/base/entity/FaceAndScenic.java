package cn.kj0601.ticket.base.entity;

import lombok.Data;


import java.util.List;

/**
 * 名称
 *
 * @Description :
 * @Author : zhangbo
 * @Date: 2021/12/16 12:25
 */
@Data
public class FaceAndScenic {
    /**
     * 主键
     */
    private String id;

    /**
     * 单价
     */
    private Integer money;

    /**
     * 名称
     */
    private String name;

    /**
     * 0.通票，1.套票 ，单票
     */
    private Integer type;

    /**
     * 图标
     */
    private String icon;

    /**
     * 详细介绍
     */
    private String conetext;

    private List<Scenic> scenic;
}
