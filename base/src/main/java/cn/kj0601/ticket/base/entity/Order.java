package cn.kj0601.ticket.base.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单表 
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("o_order")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    /**
     * 订单数量
     */
    private String orderNum;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 票务id
     */
    private String ticketId;

    /**
     * 钱数
     */
    private Integer money;

    /**
     * 0.待支付，1.已支付，2.已完成，3.已改签，4.退票中，5.退票完成，6.已过期
     */
    private Integer state;

    /**
     * 支付流水号
     */
    private String payNum;

    /**
     * 0.支付宝，1.微信，2.银联
     */
    private Integer payType;

    /**
     * 支付时间
     */
    private LocalDateTime payTime;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 删除时间
     */
    private LocalDateTime updateTime;

    /**
     * 0.未删除，1.已删除
     */
    private Integer delState;


}
