package cn.kj0601.ticket.app.controller;


import cn.kj0601.ticket.app.service.FaceScenicService;
import cn.kj0601.ticket.base.util.ResultJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * <p>
 * 票和景点 前端控制器
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@RestController
@RequestMapping("/faceScenic")
public class FaceScenicController {

    @Autowired
    FaceScenicService faceScenicService;

    @PostMapping("list")
    public ResultJson list(@RequestParam Map<String,Object> map){

        if(map.get("id") == null || "".equals(map.get("id"))){
            return ResultJson.err(203,"id未传递");
        }
        return faceScenicService.selectTicket(map);
    }

}

