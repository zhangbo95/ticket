package cn.kj0601.ticket.app.service.impl;

import cn.kj0601.ticket.base.dao.*;
import cn.kj0601.ticket.base.entity.Order;
import cn.kj0601.ticket.app.service.OrderService;
import cn.kj0601.ticket.base.entity.OrderTicket;
import cn.kj0601.ticket.base.entity.OrderTourist;
import cn.kj0601.ticket.base.entity.Ticket;
import cn.kj0601.ticket.base.util.ResultJson;
import cn.kj0601.ticket.base.util.UUIDUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单表  服务实现类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderDao, Order> implements OrderService {

    @Autowired
    TicketDao ticketDao;//票务

    @Autowired
    FaceDao faceDao;//票面

    @Autowired
    OrderDao orderDao;//订单

    @Autowired
    OrderTicketDao orderTicketDao;//订单票务关系

    @Autowired
    OrderTouristDao orderTouristDao;//订单游客关系

    @Autowired
    TouristDao touristDao;//游客信息


    @Override
    @Transactional
    public ResultJson addOrder(Map<String, Object> map) {
        //从前端获取 faceid 的集合列表  和游客信息tourist集合列表
        List<String> faceIdList = Arrays.asList(map.get("faceIds").toString().split(","));
        List<String> touristIdList =Arrays.asList(map.get("touristIds").toString().split(","));

        //查询票务数量是否充足
        //修改票务数量
        Integer totalPriceOne = 0;  //记录每张票的价格 累加
        List<Ticket> ticketList = new ArrayList<>(); //创建票务集合  存放 购买的票务  用于后期生成票务_订单信息
        QueryWrapper<Ticket> queryWrapper = new QueryWrapper<>();
        for(int i=0;i<=faceIdList.size();i++){
            queryWrapper.eq("face_id",faceIdList.get(i))
                    .eq("open_time",map.get("openTime").toString())
                    .eq("del_state",0);

            Ticket ticket = ticketDao.selectOne(queryWrapper);
            if(ticket==null){
                return ResultJson.err(203,"暂无票务信息");
            }
            if(ticket.getTicketNum()<touristIdList.size()){
                return ResultJson.err(203,"余票不足，请重新选择");
            }
            map.put("faceId",faceIdList.get(i));
            map.put("buyNum",touristIdList.size());
            if(ticketDao.updateTicketNum(map)!=1){
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ResultJson.err(203,ticket.getId()+"修改失败");
            }
            totalPriceOne+=ticket.getPrice();
        }
        //计算总价格
        Integer totalPrice = totalPriceOne*touristIdList.size();


        Order order = new Order();
        order.setId(UUIDUtil.getUUID())
                .setOrderNum(UUIDUtil.getUUID())
                .setCreateTime(LocalDateTime.now())
                .setUpdateTime(LocalDateTime.now())
                .setMoney(totalPrice)
                .setUserId(map.get("userId").toString());

        //创建订单
        if(orderDao.insert(order)!=1){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ResultJson.err(203,"新建订单失败");
        }
        //订单创建完成后  创建订单与票务关系
        OrderTicket orderTicket = new OrderTicket();
        for(int i=0;i<ticketList.size();i++){
            orderTicket.setId(UUIDUtil.getUUID())
                    .setCreateTime(LocalDateTime.now())
                    .setOrderId(order.getId())
                    .setUpdateTime(LocalDateTime.now())
                    .setFullNum(totalPriceOne*touristIdList.size())
                    .setTicketId(ticketList.get(i).getId());

            if(orderTicketDao.insert(orderTicket)!=1){  //同样 循环新增可能有bug  此条回滚  上条已经添加成功
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ResultJson.err(203,ticketList.get(i).getId()+"新增失败");
            }

        }

        //生成游客_订单信息
        OrderTourist orderTourist = new OrderTourist();
        orderTourist.setOrderId(order.getId())
                .setCreateTime(LocalDateTime.now())
                .setUpdateTime(LocalDateTime.now());
        for(int i=0;i<touristIdList.size();i++){
            orderTourist.setTouristId(touristIdList.get(i))
                    .setId(UUIDUtil.getUUID());
            if(orderTouristDao.insert(orderTourist)!=1){   //循环增加可能会出现bug
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ResultJson.err(203,orderTourist.getTouristId()+"新增失败");
            }
        }

        return ResultJson.ok("ok",order.getId());
    }
}
