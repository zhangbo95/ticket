package cn.kj0601.ticket.app.service.impl;

import cn.kj0601.ticket.base.entity.Auth;
import cn.kj0601.ticket.base.dao.AuthDao;
import cn.kj0601.ticket.app.service.AuthService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@Service
public class AuthServiceImpl extends ServiceImpl<AuthDao, Auth> implements AuthService {

}
