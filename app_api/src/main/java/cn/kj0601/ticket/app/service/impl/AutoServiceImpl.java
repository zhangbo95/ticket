package cn.kj0601.ticket.app.service.impl;

import cn.kj0601.ticket.base.entity.Auto;
import cn.kj0601.ticket.base.dao.AutoDao;
import cn.kj0601.ticket.app.service.AutoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 管理 服务实现类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@Service
public class AutoServiceImpl extends ServiceImpl<AutoDao, Auto> implements AutoService {

}
