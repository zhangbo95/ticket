package cn.kj0601.ticket.app.service;

import cn.kj0601.ticket.base.entity.Info;
import cn.kj0601.ticket.base.util.ResultJson;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 信息表 服务类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
public interface InfoService extends IService<Info> {
    //轮播图
    ResultJson SelectInquire(Map<String, Object> map);
    //景点
    ResultJson selecetScenicspot(Map<String,Object> map);
    //优惠信息
    ResultJson SelectDiscounts(Map<String, Object> map);
    //园区信息
    ResultJson SelectGarden(Map<String, Object> map);



}
