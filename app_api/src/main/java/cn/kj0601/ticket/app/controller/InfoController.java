package cn.kj0601.ticket.app.controller;


import cn.kj0601.ticket.app.service.InfoService;
import cn.kj0601.ticket.app.service.ScenicService;
import cn.kj0601.ticket.base.util.ResultJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 信息表 前端控制器
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@RestController
@RequestMapping("/info")
public class InfoController {

    @Autowired
    InfoService infoService;

    @PostMapping("getInfo")
    public ResultJson get(@RequestParam Map<String,Object> map){
        if(map.get("type").equals("0")){
            //轮播图-通过时间倒序查询三个
            return infoService.SelectInquire(map);
        }
        if(map.get("type").equals("1")){
            //景点-查询最新的3个
            return infoService.selecetScenicspot(map);
        }
        if(map.get("type").equals("2")){
            //优惠信息-查询最新的2个
            return infoService.SelectDiscounts(map);
        }
        if(map.get("type").equals("3")){
            //园区信息-查询最新的一个
            return infoService.SelectGarden(map);
        }
        return ResultJson.err(203,"错误");
    }



}

