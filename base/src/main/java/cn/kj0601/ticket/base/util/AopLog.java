package cn.kj0601.ticket.base.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * KJ0601
 *
 * @Description : 这是处理aop日志的
 * @Author : zhangbo
 * @Date: 2021/11/13 8:53
 * 1.切点
 * 2.处理切面
 */
@Slf4j
@Aspect
@Component
public class AopLog {

    static ObjectMapper om = new ObjectMapper();
    /**
    * 参数介绍：
    * @description:  切点定义
    * @author: Aedes
    * @date: 2021/11/13 8:58
    * @return: void
    **/
    @Pointcut("execution(public * cn.kj0601.ticket..controller.*.*(..))")
    public void controllerAop(){
    }


    /**
    * 参数介绍：
    * @description: 进入方法的切面
    * @author: Aedes
    * @date: 2021/11/13 9:02
    * @return: void
    **/
    @Before("controllerAop()")
    public void beforeAop(JoinPoint joinPoint){
        log.info("=====================访问进入=====================");
        //获取HttpServletRequest
        ServletRequestAttributes ra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = ra.getRequest();

        //完成日志处理
        log.info("访问ip为："+request.getRemoteAddr());
        log.info("访问地址："+request.getRequestURL());
        log.info("访问方式："+request.getMethod());
        log.info("处理方法："+joinPoint.getTarget().toString()+":"+joinPoint.getSignature().getName());
        log.info("携带参数："+ Arrays.toString(joinPoint.getArgs()));
    }

    /**
    * 参数介绍：
    * @description: 返回前的织入
    * @author: Aedes
    * @date: 2021/11/13 9:26
    * @return: void
    **/
    @AfterReturning(pointcut = "controllerAop()",returning = "obj")
    public void afterAop(Object obj) throws JsonProcessingException {

        log.info(om.writeValueAsString(obj));
        log.info("=====================访问结束=====================");
    }



}


