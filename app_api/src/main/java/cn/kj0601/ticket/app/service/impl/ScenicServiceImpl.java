package cn.kj0601.ticket.app.service.impl;

import cn.kj0601.ticket.base.entity.Scenic;
import cn.kj0601.ticket.base.dao.ScenicDao;
import cn.kj0601.ticket.app.service.ScenicService;
import cn.kj0601.ticket.base.util.ResultJson;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 景点表 服务实现类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@Service
public class ScenicServiceImpl extends ServiceImpl<ScenicDao, Scenic> implements ScenicService {


}
