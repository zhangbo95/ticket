package cn.kj0601.ticket.app.service.impl;

import cn.hutool.core.date.DateTime;
import cn.kj0601.ticket.base.entity.Info;
import cn.kj0601.ticket.base.entity.Temp;
import cn.kj0601.ticket.base.dao.TempDao;
import cn.kj0601.ticket.app.service.TempService;
import cn.kj0601.ticket.base.util.ResultJson;
import cn.kj0601.ticket.base.util.UUIDUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * <p>
 * 订单临时表  服务实现类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@Service
public class TempServiceImpl extends ServiceImpl<TempDao, Temp> implements TempService {

    @Autowired
    TempDao tempDao;

    @Override
    public ResultJson addTemp(Map<String, Object> map) {
        Temp temp = new Temp();
        temp.setId(UUIDUtil.getUUID());
        temp.setFaceId(map.get("face_id").toString());
        temp.setUserId(map.get("user_id").toString());
        temp.setCreateTime(LocalDateTime.now());
        temp.setUpdateTime(LocalDateTime.now());

        int num = tempDao.insert(temp);
        if(num <= 0){
            return ResultJson.err(203,"存入失败");
        }

        Temp reTemp = tempDao.selectOne(new QueryWrapper<Temp>().eq("id",temp.getId()));

        return ResultJson.ok("存入成功",reTemp);
    }
}
