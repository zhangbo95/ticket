package cn.kj0601.ticket.base.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单票务关系表
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("o_order_ticket")
public class OrderTicket implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    /**
     * 订单id
     */
    private String orderId;

    /**
     * 票务id
     */
    private String ticketId;

    /**
     * 0.未使用，1.已使用，2.已改签，3.退票中，4.退票完成
     */
    private Integer state;

    /**
     * 全票人数
     */
    private Integer fullNum;

    /**
     * 半票人数
     */
    private Integer halfNum;

    /**
     * 已使用全票人数
     */
    private Integer useFullNum;

    /**
     * 已使用半票人数
     */
    private Integer useHalfNum;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 删除时间
     */
    private LocalDateTime updateTime;

    /**
     * 0.未删除，1.已删除
     */
    private Integer delState;


}
