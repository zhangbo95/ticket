package cn.kj0601.ticket.app.service;

import cn.kj0601.ticket.base.entity.Scenic;
import cn.kj0601.ticket.base.util.ResultJson;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 景点表 服务类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
public interface ScenicService extends IService<Scenic> {



}
