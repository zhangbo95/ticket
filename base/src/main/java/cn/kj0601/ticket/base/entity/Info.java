package cn.kj0601.ticket.base.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 信息表
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("p_info")
public class Info implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    /**
     * 0.轮播,1.热门景点，2.优惠信息，3园区信息
     */
    private Integer type;

    /**
     * 标题
     */
    private String title;

    /**
     * 图标
     */
    private String icon;

    /**
     * 内容
     */
    private String context;

    /**
     * 0.内容，1.门票 ，2.网页
     */
    private Integer urlType;

    /**
     * 跳转门票表的id
     */
    private String urlId;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 删除时间
     */
    private LocalDateTime updateTime;

    /**
     * 0.未删除，1.已删除
     */
    private Integer delState;


}
