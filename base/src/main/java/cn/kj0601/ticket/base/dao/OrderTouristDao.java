package cn.kj0601.ticket.base.dao;

import cn.kj0601.ticket.base.entity.OrderTourist;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单游客关系表 Mapper 接口
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
public interface OrderTouristDao extends BaseMapper<OrderTourist> {

}
