package cn.kj0601.ticket.app.service.impl;

import cn.kj0601.ticket.base.entity.Face;
import cn.kj0601.ticket.base.dao.FaceDao;
import cn.kj0601.ticket.app.service.FaceService;
import cn.kj0601.ticket.base.entity.FaceAndScenic;
import cn.kj0601.ticket.base.util.ResultJson;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 票面表 服务实现类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@Service
public class FaceServiceImpl extends ServiceImpl<FaceDao, Face> implements FaceService {

    @Autowired
    FaceDao faceDao;


    //票务列表
    @Override
    public ResultJson selectTicket(Map<String, Object> map) {

        List<Face> list = faceDao.selectByMap(map);
        return ResultJson.ok("票务列表",list);
    }




}
