package cn.kj0601.ticket.base.dao;

import cn.kj0601.ticket.base.entity.Ticket;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.Map;

/**
 * <p>
 * 票务 Mapper 接口
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
public interface TicketDao extends BaseMapper<Ticket> {
    //修改票数
    int updateTicketNum(Map<String,Object> map);

}
