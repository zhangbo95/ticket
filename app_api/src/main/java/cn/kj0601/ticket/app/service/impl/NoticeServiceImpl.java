package cn.kj0601.ticket.app.service.impl;

import cn.kj0601.ticket.base.entity.Notice;
import cn.kj0601.ticket.base.dao.NoticeDao;
import cn.kj0601.ticket.app.service.NoticeService;
import cn.kj0601.ticket.base.util.ResultJson;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 公告表 服务实现类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeDao, Notice> implements NoticeService {
    @Autowired
    NoticeDao noticeDao;

    @Override
    public ResultJson select(Map<String, Object> map) {

        QueryWrapper<Notice> queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("update_time").eq("del_state",0);
        queryWrapper.last("limit 1");
        Notice notice = noticeDao.selectOne(queryWrapper);
        return ResultJson.ok(notice);
    }
}
