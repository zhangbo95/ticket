package cn.kj0601.ticket.app.service.impl;

import cn.kj0601.ticket.base.entity.Tourist;
import cn.kj0601.ticket.base.dao.TouristDao;
import cn.kj0601.ticket.app.service.TouristService;
import cn.kj0601.ticket.base.util.ResultJson;
import cn.kj0601.ticket.base.util.UUIDUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 游客表 服务实现类
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@Service
public class TouristServiceImpl extends ServiceImpl<TouristDao, Tourist> implements TouristService {

    @Autowired
    TouristDao touristDao;

    @Override
    public ResultJson addTourist(Map<String, Object> map) {
        map.put("id", UUIDUtil.getUUID());
        int num = touristDao.insertTourist(map);
        if(num==0){
            return ResultJson.err(203,"添加失败，联系人或已存在");
        }
        return ResultJson.ok("添加成功");
    }
}
