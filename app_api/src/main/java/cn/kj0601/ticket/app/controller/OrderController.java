package cn.kj0601.ticket.app.controller;


import cn.kj0601.ticket.app.service.OrderService;
import cn.kj0601.ticket.base.util.ResultJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * <p>
 * 订单表  前端控制器
 * </p>
 *
 * @author kj0601
 * @since 2021-12-14
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    OrderService orderService;

    @PostMapping("addlist")
    public ResultJson addOrder(@RequestParam Map<String,Object> map){
        if(map.get("userId")==null || "".equals(map.get("userId"))){
            return ResultJson.err(201,"请传入用户id");
        }
        if(map.get("openTime")==null || "".equals(map.get("openTime"))){
            return ResultJson.err(201,"请选择如园时间");
        }
        if(map.get("faceIds")==null || "".equals(map.get("faceIds"))){
            return ResultJson.err(201,"请选票");
        }
        if(map.get("touristIds")==null || "".equals(map.get("touristIds"))){
            return ResultJson.err(201,"请传入游客信息");
        }
        return orderService.addOrder(map);

    }

}

